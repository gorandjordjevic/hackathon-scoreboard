import React, { Component } from 'react';

import Shooter from './Shooter';

class ViewScoreBoard extends Component {

	dataSource() {
		return [
			{
				'ime': 'Petar',
				'prezime': 'Petrović',
				'nacionalnost': 'SRB',
				'rounds': [
					21,
					22,
					23,
					24,
					25
				]
			},
			{
				'ime': 'Marko',
				'prezime': 'Marković',
				'nacionalnost': 'CRO',
				'rounds': [
					25,
					24,
					23,
					22,
					21
				]
			},
			{
				'ime': 'Stefan',
				'prezime': 'Stefanović',
				'nacionalnost': 'BiH',
				'rounds': [
					25,
					24,
					22,
					23,
					21
				]
			}
		];
	}

	render() {

		let $dataSource = this.dataSource();

		let $elements = [];

		$dataSource.forEach( function ( $obj, $index ) {
			$elements.push(
					<Shooter key={$index}
									 ime={$obj.ime}
									 prezime={$obj.prezime}
									 nacionalnost={$obj.nacionalnost}
									 rounds={$obj.rounds}/>
			);
		} );

		return (
				<div>
					<h1>All Shooters</h1>
					<table className={"view-table"}>
						<tbody>
						<tr>
							<th>Ime</th>
							<th>Prezime</th>
							<th>Nacionalnost</th>
							<th>Runda 1</th>
							<th>Runda 2</th>
							<th>Runda 3</th>
							<th>Runda 4</th>
							<th>Runda 5</th>
						</tr>
						{$elements}
						</tbody>
					</table>
				</div>
		);
	}
}

export default ViewScoreBoard;