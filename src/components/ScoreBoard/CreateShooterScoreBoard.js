import React, { Component } from 'react';
import Input from "./Input";

class CreateShooterScoreBoard extends Component {
	render() {
		return (
				<form>
					<h1>Create</h1>
					<Input id={"firstName"}
								 type={"text"}
								 label={"First Name"}/>

					<Input id={"lastName"}
								 type={"text"}
								 label={"Last Name"}/>

					<Input id={"country"}
								 type={"text"}
								 label={"Country"}/>

					<Input type={"submit"}
								 label={"Add"}/>
				</form>
		);
	}
}

export default CreateShooterScoreBoard;