import React, { Component } from 'react';

import ViewScoreBoard from './ViewScoreBoard';
import CreateShooterScoreBoard from './CreateShooterScoreBoard';

class ScoreBoard extends Component {
	render() {
		return (
				<div>
					<ViewScoreBoard/>
					<CreateShooterScoreBoard/>
				</div>
		);
	}
}

export default ScoreBoard;