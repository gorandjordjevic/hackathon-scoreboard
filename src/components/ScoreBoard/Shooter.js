import React, { Component } from 'react';

class Shooter extends Component {

	constructor( props ) {
		super( props );

		this.state = {
			ime: this.props.ime,
			prezime: this.props.prezime,
			nacionalnost: this.props.nacionalnost,
			rounds: this.props.rounds
		};
	}

	render() {
		let $rounds = this.state.rounds;
		let $printRounds = [];
		$rounds.forEach( function ( $round, $index ) {
			$printRounds.push( <td key={$index} className={"round"}>{$round}</td> );
		} );
		return (
				<tr>
					<td className="ime">{this.state.ime}</td>
					<td className="prezime">{this.state.prezime}</td>
					<td className="nacionalnost">{this.state.nacionalnost}</td>
					{$printRounds}
				</tr>
		);
	}
}

export default Shooter;