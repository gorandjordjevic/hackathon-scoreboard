import React, { Component } from 'react';
import Header from './components/Header/Header';
import ScoreBoard from './components/ScoreBoard/ScoreBoard';

class App extends Component {
	render() {
		return (
				<div>
					<Header/>
					<ScoreBoard/>
				</div>
		);
	}
}

export default App;
