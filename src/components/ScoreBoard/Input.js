import React, { Component } from 'react';

class Input extends Component {

	constructor( props ) {
		super( props );

		this.state = {
			type: this.props.type,
			label: this.props.label,
			id: this.props.id
		};
	}

	render() {
		if ( this.state.type === "text" ) {
			return (
					<div className="element">
						<label htmlFor={this.state.id}>{this.state.label}</label>
						<input type={this.state.type} id={this.state.id}/>
					</div>
			);
		} else if ( this.state.type === "submit" ) {
			return (
					<div className="element">
						<input type={this.state.type} value={this.state.label}/>
					</div>
			);
		}
	}
}

export default Input;