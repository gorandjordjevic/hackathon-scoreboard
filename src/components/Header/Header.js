import React, { Component } from 'react';

import Logo from './assets/imgs/logo.png';
import Avatar from './assets/imgs/avatar.png';

import './assets/Header.css';

class Header extends Component {

	render() {
		return (
				<header>
					<img className={"logo"} alt={"logo"} src={Logo}/>
					<img className={"avatar"} alt={"avatar"} src={Avatar}/>
				</header>
		);
	}
}

export default Header;